package pl.bartoszosssowski;

final class CreditCard {

    private CreditCard() {
    }

    static String maskify(String cc) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < cc.length(); i++) {
            char character = cc.charAt(i);

            if (i > 0 && i < cc.length() - 4 && Character.isDigit(cc.charAt(i))) {
                output.append("#");
            }
            else{
                output.append(character);
            }


        }

        return output.toString();
    }
}

