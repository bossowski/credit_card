package pl.bartoszosssowski;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class CreditCardTest {

    @Test
    public void shouldMaskDigitsForBasicCreditCards() {
        assertEquals("5###########0694", CreditCard.maskify("5512103073210694"));
    }

    @Test
    public void shouldNotMaskHyphenInCreditCards() {
        assertEquals("4###-####-####-5616", CreditCard.maskify("4556-3646-0793-5616"));
    }

    @Test
    public void shouldNotMaskSpacesInCreditCards() {
        assertEquals("4### #### #### 5616", CreditCard.maskify("4556 3646 0793 5616"));
    }

    @Test
    public void shouldNotMaskDigitsForShortCreditCards() {
        assertEquals("54321", CreditCard.maskify("54321"));
    }

    @Test
    public void shouldHandleEmptyString() {
        assertEquals("", CreditCard.maskify(""));
    }

    @Test
    public void shouldNotMaskLetters() {
        assertEquals("Skippy", CreditCard.maskify("Skippy"));
    }
}
