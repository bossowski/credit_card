# credit_card

## Task

Usually when you buy something, you're asked whether your credit card number,
phone number or answer to your most secret question is still correct.
However, since someone could look over your shoulder, you don't want that shown on
your screen. Instead, we mask it.

Your task is to write a function `maskify`, which masks all but the first and last four characters with '#'.  
Non-digits should never be masked.

## Examples

| Input                   | Output                  | Comments                           |
| ----------------------- | ----------------------- | ---------------------------------- |
| `"4556364607935616"`    | `"4###########5616"`    |                                    |
| `"4556-3646-0793-5616"` | `"4###-####-####-5616"` |                                    |
| `"646079355616"`        | `"6######5616"`         |                                    |
| `"12345"`               | `"12345"`               | No `#`s, it less than 6 characters |
| `""`                    | `""`                    | Make sure to handle empty strings  |
| `"Skippy"`              | `"Skippy"`              |                                    |

## Documentation

#### maskify(cc)

#### Parameters:
**cc: `String`**  
A string of any characters.

*Guaranteed Constraints:*  
* The input string will never be `null`.

#### Returns: **`String`**
The input string with all but the first and last four characters replaced with `'#'`.
